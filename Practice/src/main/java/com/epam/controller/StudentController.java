package com.epam.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.StudentDTO;
import com.epam.service.StudentService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/students")
public class StudentController {
	
	private final StudentService studentService;
	
	@PostMapping
	public ResponseEntity<StudentDTO> add(@RequestBody StudentDTO studentDTO){
		return new ResponseEntity<>(studentService.add(studentDTO),HttpStatus.CREATED);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> remove(@PathVariable int id){
		studentService.remove(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<StudentDTO> update(@RequestBody StudentDTO studentDTO, @PathVariable int id){
		return new ResponseEntity<>(studentService.update(studentDTO, id),HttpStatus.OK);
	}
	
	@GetMapping
	public ResponseEntity<List<StudentDTO>> fetch(){
		return new ResponseEntity<>(studentService.fetch(),HttpStatus.OK);
	}

}
