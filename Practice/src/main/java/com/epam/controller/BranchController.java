package com.epam.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.BranchDTO;
import com.epam.service.BranchService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/branches")
@RequiredArgsConstructor
public class BranchController {
	
	private final BranchService branchService;
	
	@PostMapping
	public ResponseEntity<BranchDTO> addBranch(@RequestBody  @Valid  BranchDTO branch){
		return new ResponseEntity<>(branchService.addBranch(branch),HttpStatus.CREATED);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> removeBranch(@PathVariable int id){
		branchService.removeBranch(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<BranchDTO> updateBranch(@PathVariable int id, @RequestBody @Valid BranchDTO branch){
		return new ResponseEntity<>(branchService.updateBranch(id, branch),HttpStatus.OK);
	}
	
	@GetMapping
	public ResponseEntity<List<BranchDTO>> fetchBranches(){
		return new ResponseEntity<>(branchService.fetchBranches(),HttpStatus.OK);
	}

}
