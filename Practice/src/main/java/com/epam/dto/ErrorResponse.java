package com.epam.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ErrorResponse {
	
	private String timeStamp;
	private String status;
	private String error;
	private String path;
	
}
