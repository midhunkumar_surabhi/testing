package com.epam.dto;

import java.util.Set;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BranchDTO {
	
	@Schema(accessMode = AccessMode.READ_ONLY)
	private Integer branchId;
	
	@NotBlank(message = "Branch should not be blank")
	@Size(min = 2, message = "Branch name should be minimum of 2 characters")
	private String branchName;
	
	@NotEmpty(message = "Students should not be empty")
	private Set<StudentDTO> students;
	
}
