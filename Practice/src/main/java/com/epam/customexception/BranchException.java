package com.epam.customexception;

public class BranchException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6068823956549424171L;
	
	public BranchException(String message) {
		super(message);
	}

}
