package com.epam.service;

import java.util.List;

import com.epam.dto.StudentDTO;

public interface StudentService {
	
	StudentDTO add (StudentDTO student);
	
	void remove (int studentId);
	
	StudentDTO update (StudentDTO student, int studentId);
	
	List<StudentDTO> fetch ();

}
