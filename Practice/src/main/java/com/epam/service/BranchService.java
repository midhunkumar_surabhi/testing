package com.epam.service;

import java.util.List;

import com.epam.dto.BranchDTO;

public interface BranchService {
	
	BranchDTO addBranch(BranchDTO branch);
	
	void removeBranch(int branchId);
	
	BranchDTO updateBranch(int branchId, BranchDTO branch);
	
	List<BranchDTO> fetchBranches();

}
