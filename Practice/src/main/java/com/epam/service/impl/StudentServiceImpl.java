package com.epam.service.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.epam.customexception.BranchException;
import com.epam.dto.StudentDTO;
import com.epam.entity.Student;
import com.epam.repository.StudentRepository;
import com.epam.service.StudentService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
	
	private final StudentRepository studentRepository;
	private final ModelMapper modelMapper;

	@Override
	public StudentDTO add(StudentDTO studentDTO) {
		Student student = studentRepository.save(modelMapper.map(studentDTO, Student.class));
		return modelMapper.map(student, StudentDTO.class);
	}

	@Override
	public void remove(int studentId) {
		studentRepository.deleteById(studentId);
	}

	@Override
	public StudentDTO update(StudentDTO studentDTO, int studentId) {
		Student student = studentRepository.findById(studentId).orElseThrow(() -> new BranchException("No student"));
		Student s = modelMapper.map(studentDTO, Student.class);
		s.setBranch(student.getBranch());
		s.setStudentId(studentId);
		return modelMapper.map(studentRepository.save(s), StudentDTO.class);
	}

	@Override
	public List<StudentDTO> fetch() {
		return studentRepository.findAll().stream().map(student -> modelMapper.map(student, StudentDTO.class)).toList();
	}

}
