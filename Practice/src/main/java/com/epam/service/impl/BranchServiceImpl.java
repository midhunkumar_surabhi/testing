package com.epam.service.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.epam.customexception.BranchException;
import com.epam.dto.BranchDTO;
import com.epam.entity.Branch;
import com.epam.repository.BranchRepository;
import com.epam.service.BranchService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService{
	
	private final BranchRepository branchRepository;
	private final ModelMapper modelMapper;
	
	@Override
	public BranchDTO addBranch(BranchDTO branchDTO) {
		Branch branch = branchRepository.save(modelMapper.map(branchDTO, Branch.class));
		return modelMapper.map(branch, BranchDTO.class);
	}
	
	@Override
	public void removeBranch(int branchId) {
		branchRepository.deleteById(branchId);
	}
	
	@Override
	public BranchDTO updateBranch(int branchId, BranchDTO branchDTO) {
				
		if(!branchRepository.existsById(branchId)) {
			throw new BranchException("Branch not found with Id : "+branchId);
		}
		
		Branch branch = modelMapper.map(branchDTO, Branch.class);
		branch.setBranchId(branchId);
		
		return modelMapper.map(branchRepository.save(branch),BranchDTO.class);
		
	}
	
	@Override
	public List<BranchDTO> fetchBranches() {
		List<Branch> branches = branchRepository.findAll();
		return branches.stream().map(branch -> modelMapper.map(branch, BranchDTO.class)).toList();
	}	

}
