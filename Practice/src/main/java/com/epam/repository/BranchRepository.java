package com.epam.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.entity.Branch;

@Repository
public interface BranchRepository extends JpaRepository<Branch, Integer> {
	Optional<Branch> findByBranchName(String branchName);
}
