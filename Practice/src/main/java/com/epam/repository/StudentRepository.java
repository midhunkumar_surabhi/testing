package com.epam.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Integer> {

}
