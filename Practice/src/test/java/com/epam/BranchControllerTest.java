package com.epam;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.controller.BranchController;
import com.epam.customexception.BranchException;
import com.epam.dto.BranchDTO;
import com.epam.dto.StudentDTO;
import com.epam.entity.Student;
import com.epam.service.BranchService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(BranchController.class)
class BranchControllerTest {
	
	
	private Student student;
	
	private BranchDTO branchDTO;
	
	private StudentDTO studentDTO;
	
	private Set<Student> students;
	
	private Set<StudentDTO> studentDtos;
	
	@MockBean
	private BranchService branchService;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void testAddBranch() throws JsonProcessingException, Exception {
		
		Mockito.when(branchService.addBranch(any(BranchDTO.class))).thenReturn(branchDTO);
		
		mockMvc.perform(post("/branches")
				      .contentType(MediaType.APPLICATION_JSON)
				      .content(new ObjectMapper().writeValueAsString(branchDTO)))
		              .andExpect(status().isCreated());
		
		Mockito.verify(branchService).addBranch(any(BranchDTO.class));
		
	}
	
	@Test
	void testAddBranchWithExistingName() throws JsonProcessingException, Exception {
		
		Mockito.when(branchService.addBranch(any(BranchDTO.class))).thenThrow(DataIntegrityViolationException.class);
		
		mockMvc.perform(post("/branches")
				      .contentType(MediaType.APPLICATION_JSON)
				      .content(new ObjectMapper().writeValueAsString(branchDTO)))
		              .andExpect(status().isBadRequest());
		
		Mockito.verify(branchService).addBranch(any(BranchDTO.class));
		
	}
	
	@Test
	void testRemoveBranch() throws Exception {
		Mockito.doNothing().when(branchService).removeBranch(1);
		mockMvc.perform(delete("/branches/{id}",1)).andExpect(status().isNoContent());
		Mockito.verify(branchService).removeBranch(1);
	}
	
	@Test
	void testRemoveBranchWithInvalidId() throws Exception {
		mockMvc.perform(delete("/branches/{id}","hi")).andExpect(status().isInternalServerError());
	}
	
	@Test
	void testupdateBranch() throws JsonProcessingException, Exception {
		
		Mockito.when(branchService.updateBranch(anyInt(),any(BranchDTO.class))).thenReturn(branchDTO);
		
		mockMvc.perform(put("/branches/{id}",1)
			      .contentType(MediaType.APPLICATION_JSON)
			      .content(new ObjectMapper().writeValueAsString(branchDTO)))
	              .andExpect(status().isOk());
		
		Mockito.verify(branchService).updateBranch(anyInt(),any(BranchDTO.class));
		
	}
	
	@Test
	void testupdateBranchWithBranchException() throws JsonProcessingException, Exception {
		
		Mockito.when(branchService.updateBranch(anyInt(),any(BranchDTO.class))).thenThrow(BranchException.class);
		
		mockMvc.perform(put("/branches/{id}",1)
			      .contentType(MediaType.APPLICATION_JSON)
			      .content(new ObjectMapper().writeValueAsString(branchDTO)))
	              .andExpect(status().isBadRequest());
		
		Mockito.verify(branchService).updateBranch(anyInt(),any(BranchDTO.class));
		
	}
	
	@Test
	void testupdateBranchWithInvalaidInput() throws JsonProcessingException, Exception {
		branchDTO = BranchDTO.builder().branchName("a").students(studentDtos).build();
		
		mockMvc.perform(put("/branches/{id}",1)
			      .contentType(MediaType.APPLICATION_JSON)
			      .content(new ObjectMapper().writeValueAsString(branchDTO)))
	              .andExpect(status().isBadRequest());
	}
	
	@Test
	void testFetchBranches() throws Exception {
		Mockito.when(branchService.fetchBranches()).thenReturn(Arrays.asList(branchDTO));
		mockMvc.perform(get("/branches")).andExpect(status().isOk());
		Mockito.verify(branchService).fetchBranches();
	}
	
	@BeforeEach
	public void setUpStudent() {
		student = Student.builder().studentId(1).studentName("Himesh").build();
		studentDTO = StudentDTO.builder().studentName("Himesh").build();
	}
	
	@BeforeEach
	public void setUpBranch() {
		students = new HashSet<>();
		studentDtos = new HashSet<>();
		students.add(student);
		studentDtos.add(studentDTO);
		branchDTO = BranchDTO.builder().branchName("ECE").students(studentDtos).build();
	}
}
