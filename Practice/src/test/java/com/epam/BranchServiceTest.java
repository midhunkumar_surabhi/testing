package com.epam;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashSet;
import java.util.Set;
import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import com.epam.customexception.BranchException;
import com.epam.dto.BranchDTO;
import com.epam.dto.StudentDTO;
import com.epam.entity.Branch;
import com.epam.entity.Student;
import com.epam.repository.BranchRepository;
import com.epam.service.impl.BranchServiceImpl;

@ExtendWith(MockitoExtension.class)
class BranchServiceTest {
	
	private Branch branch;
	
	private Student student;
	
	private BranchDTO branchDTO;
	
	private StudentDTO studentDTO;
	
	private Set<Student> students;
	
	private Set<StudentDTO> studentDtos;
	
	@Mock
	private BranchRepository branchRepository;
	
	@Mock
	private ModelMapper modelMapper;
	
	@InjectMocks
	private BranchServiceImpl branchService;
	
	@Test
	void testAddBranch() {
		Mockito.when(branchRepository.save(branch)).thenReturn(branch);
		Mockito.when(modelMapper.map(branch, BranchDTO.class)).thenReturn(branchDTO);
		Mockito.when(modelMapper.map(branchDTO, Branch.class)).thenReturn(branch);
		assertEquals(branchDTO, branchService.addBranch(branchDTO));
		Mockito.verify(modelMapper).map(branchDTO, Branch.class);
		Mockito.verify(modelMapper).map(branch, BranchDTO.class);
		Mockito.verify(branchRepository).save(branch);
	}
	
	@Test
	void testRemoveBranch() {
		Mockito.doNothing().when(branchRepository).deleteById(1);
		branchService.removeBranch(1);
		Mockito.verify(branchRepository).deleteById(1);
		
	}
	
	@Test
	void testUpdateBranch() {
		Mockito.when(branchRepository.save(branch)).thenReturn(branch);
		Mockito.when(modelMapper.map(branch, BranchDTO.class)).thenReturn(branchDTO);
		Mockito.when(modelMapper.map(branchDTO, Branch.class)).thenReturn(branch);
		Mockito.when(branchRepository.existsById(1)).thenReturn(true);
		assertEquals(branchDTO, branchService.updateBranch(1, branchDTO));
		Mockito.verify(modelMapper).map(branchDTO, Branch.class);
		Mockito.verify(modelMapper).map(branch, BranchDTO.class);
		Mockito.verify(branchRepository).save(branch);
		Mockito.verify(branchRepository).existsById(1);
	}
	
	@Test
	void testUpdateBranchWithException() {
		Mockito.when(branchRepository.existsById(1)).thenReturn(false);
		assertThrows(BranchException.class, () -> branchService.updateBranch(1, branchDTO));
		Mockito.verify(branchRepository).existsById(1);
	}
	
	@Test
	void testFetchBranches() {
		Mockito.when(branchRepository.findAll()).thenReturn(Arrays.asList(branch));
		Mockito.when(modelMapper.map(branch, BranchDTO.class)).thenReturn(branchDTO);
		assertEquals(Arrays.asList(branchDTO),branchService.fetchBranches());
		Mockito.verify(modelMapper).map(branch, BranchDTO.class);
		Mockito.verify(branchRepository).findAll();
	}
	
	@BeforeEach
	public void setUpStudent() {
		student = Student.builder().studentId(1).studentName("Himesh").build();
		studentDTO = StudentDTO.builder().studentName("Himesh").build();
	}
	
	@BeforeEach
	public void setUpBranch() {
		students = new HashSet<>();
		studentDtos = new HashSet<>();
		students.add(student);
		studentDtos.add(studentDTO);
		branch = Branch.builder().branchId(1).branchName("ECE").students(students).build();
		branchDTO = BranchDTO.builder().branchName("ECE").students(studentDtos).build();
	}

}
